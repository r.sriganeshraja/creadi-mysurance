'use strict';
import styles from './application/styles/styles';
import InsuranceWelcome from './application/pages/InsuranceWelcome';
import React, { Component } from 'react';
import { AppRegistry, NavigatorIOS } from 'react-native';
import SplashScreen from 'react-native-splash-screen';

class MySurance extends Component {
  componentDidMount() {
    SplashScreen.hide();
  }
  render() {
    return (
      <NavigatorIOS
        style={styles.navigator}
        initialRoute={{component: InsuranceWelcome, title: 'MySurance'}}/>
    );
  }
}

AppRegistry.registerComponent('mysurance', () => MySurance);
