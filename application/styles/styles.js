'use strict';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';

var styles = StyleSheet.create({

    navigator: {flex: 1},

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 10,
        backgroundColor: '#ffffff',
    },

    welcomeText: {
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        textAlign:'center',
        alignSelf: 'stretch',
        justifyContent:'center',
        color: '#000',
        fontSize: 40,
        marginTop: 300
    },
	
	total: {
		textAlign: 'center',
		fontWeight: 'bold',
		fontSize: 16,
		marginTop: 70
	},

    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    },

    button: {
        height: 36,
        backgroundColor: '#48BBEC',
        alignSelf: 'stretch',
        justifyContent: 'center',
    },

    saveButton: {
        borderColor: '#48BBEC',
        borderWidth: 1,
        borderRadius: 8,
    },

    newButton: {
        marginBottom: 10,
        marginLeft:10,
        marginRight:10,
        borderRadius: 18
    },

    mysurance: {
        marginTop: 100,
        flex: 1,
        padding: 10,
        backgroundColor: '#ffffff',
    },

    txt: {
        fontSize: 18,
        marginLeft: 5,
        marginTop: 2,
        color: '#222222',
    },

    completed: {
        color: '#cccccc'
    },

    hr: {
        backgroundColor: 'rgba(0, 0, 0, 0.4)',
        height: 1,
        marginLeft: 0,
        marginRight: 0,
    },

});


module.exports = styles;
