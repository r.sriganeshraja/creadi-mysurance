'use strict';
import styles from '../styles/styles';
import React, { Component } from 'react';
import { Text, View, ListView, TouchableHighlight, AlertIOS } from 'react-native';
import InsuranceListContainer from './InsuranceListContainer';

class InsuranceWelcome extends Component {
  constructor() {
    super();
  }
  onPositive(){
    this.props.navigator.push({
        title: 'My Insurance List',
        component: InsuranceListContainer,
        navigationBarHidden: false,
        passProps: {}
    });
  }
  render() {
    return (
      <View style={{flex:1}}>
        <View style={styles.container}>
          <Text style={styles.welcomeText}>Welcome</Text>
          <TouchableHighlight
            style={[styles.button, styles.newButton]}
            underlayColor='#99d9f4'
            onPress={ () => this.onPositive() }>
            <Text style={styles.buttonText}>Let`s Go</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

module.exports = InsuranceWelcome;
