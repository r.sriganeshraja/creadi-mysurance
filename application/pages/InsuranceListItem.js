'use strict';
var styles = require('../styles/styles');
import React, { Component } from 'react';
import { Text, View, TouchableHighlight } from 'react-native';


class InsuranceListItem extends Component {
    render() {
        let item = this.props.item;
        return (
            <View>
                <TouchableHighlight
                    onPress={this.props.onPress}
                    onLongPress={this.props.onLongPress}>
                    <View style={styles.container}>
                        <Text>
                            {item.title}
                        </Text>
                    </View>
                </TouchableHighlight>
                <View style={styles.hr}/>
            </View>
        );
    }
}

module.exports = InsuranceListItem;
