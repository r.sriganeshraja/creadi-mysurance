'use strict';
import styles from '../styles/styles';
import React, { Component } from 'react';
import { Text, View, ListView, TouchableHighlight, AlertIOS } from 'react-native';
import InsuranceList from './InsuranceList';
import InsuranceEdit from './InsuranceEdit';
var t = require('tcomb-form-native');

class InsuranceListContainer extends Component {
    constructor() {
        super();
        this.state = {
            items: [],
			total: 0
        };
        this.alertMenu = this.alertMenu.bind(this);
        this.deleteItem = this.deleteItem.bind(this);
        this.updateItem = this.updateItem.bind(this);
        this.openItem = this.openItem.bind(this);

    }

    alertMenu(rowData, rowID) {
        AlertIOS.alert(
            'Quick Menu',
            null,
            [
                {text: 'Delete', onPress: () => this.deleteItem(rowData, rowID)},
                {text: 'Edit', onPress: () => this.openItem(rowData, rowID)},
                {text: 'Cancel'}
            ]
        );
    }

    deleteItem(item, index) {
        var items = this.state.items;
		var total = this.state.total;
        items.splice(index, 1);
		total -= item.cost;
        this.setState({items: items, total: total});
    }

    updateItem(item, index) {
        var items = this.state.items;
		var total = this.state.total;
        if (index) {
            items[index] = item;
        }
        else {
            items.push(item);
			total += item.cost;
        }
        this.setState({items: items, total: total});
        this.props.navigator.pop();
    }

    openItem(rowData, rowID) {
        //let staticRowData = {title: 'test', cost: 255};
        this.props.navigator.push({
            title: rowData && rowData.title || 'New Item',
            component: InsuranceEdit,
            passProps: {item: rowData, id: rowID, update: this.updateItem}
        });
    }

    render() {
        return (
            <View style={{flex:1}}>
				<Text style={styles.total}>Total: {this.state.total}</Text>
                <InsuranceList
                    items={this.state.items}
                    onPressItem={this.openItem}
                    onLongPressItem={this.alertMenu}/>
                <TouchableHighlight
                    style={[styles.button, styles.newButton]}
                    underlayColor='#99d9f4'
                    onPress={this.openItem}>
                    <Text style={styles.buttonText}>Add Insurance</Text>
                </TouchableHighlight>
            </View>
        );
    }
}

module.exports = InsuranceListContainer;
