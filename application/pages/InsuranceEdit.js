'use strict';
var styles = require('../styles/styles');
import React, { Component } from 'react';
var t = require('tcomb-form-native');
import { View, TouchableHighlight, Text, Picker } from 'react-native';
var Form = t.form.Form;

var api = require('../services/ApiServices');

class InsuranceEdit extends Component {
    constructor(props) {
        super(props);
        this.state = {
          category: {}
        };
        this.onUpdate = this.onUpdate.bind(this);
    }

    componentWillMount() {
      api.getCategory().then((res) => {
        var dataReq = res.query.categorymembers;
        var returns = {};
        dataReq.map((el, key) => {
            var title = el.title.replace("Category:", "");
            returns[key] = title;
        });
        this.setState({
          category: returns
        });
      });
    }

    getForm() {
        return(t.struct({
            title: t.String,
            cost: t.Number,
            category: t.enums(this.state.category, "Category")
        }));
    }

    getFormOptions() {
        let selectedCat = this.props.item.category || 0;
        return({
            fields: {
                title: {
                    label: 'MyInsurance Item',
                    placeholder: 'Contract title here',
                    autoFocus: true
                },
                cost: {
                    label: 'Yearly Cost',
                    placeholder: 'Contract Cost here'
                },
                category: {
                    label: 'Category',
                    nullOption: {value: selectedCat, text: this.state.category[selectedCat]}
                }
            }
        });
    }

    onUpdate() {
        var value = this.refs.form.getValue();
        if (value) {
            this.props.update(value, this.props.id);
        }
    }

    _onChange(value) {
        console.log(value);
    }

    render() {
        return (
            <View style={styles.mysurance}>
                <Form
                    ref="form"
                    type={this.getForm()}
                    options={this.getFormOptions()}
                    value={this.props.item}/>
                <TouchableHighlight
                    style={[styles.button, styles.saveButton]}
                    onPress={this.onUpdate}
                    underlayColor='#99d9f4'>
                    <Text style={styles.buttonText}>Save</Text>
                </TouchableHighlight>
            </View>
        )
    }
}


module.exports = InsuranceEdit;
