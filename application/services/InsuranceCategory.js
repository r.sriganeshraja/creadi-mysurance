'use strict';
import React, { Component } from 'react';
import { View, TouchableHighlight, Text } from 'react-native';

async function category() {
    return await fetch('https://en.wikipedia.org/w/api.php?action=query&list=categorymembers&cmtitle=Category:Types_of_insurance&cmtype=subcat&format=json',  {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        }
    }).then((response) => {
        return response.json().then((data) => {
            var returns = {};
            var dataReq = data.query.categorymembers;
            dataReq.map((el, key) => {
                returns[el.title] = el.title;
            });
            return returns;
        });
    });
}

module.exports = {
    category : category
};